from src import lib
import math
import pytest


@pytest.fixture
def test_func_1():

    def function(a, x):
        return a*x**2

    return function


@pytest.fixture
def test_func_2():

    def function(a, x):
        return 1 / math.log(a * x + 1)

    return function


def test_simpsons_method_1(test_func_1):
    result = lib.simpson_method(test_func_1, 1, 0, 2*math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_simpsons_method_2(test_func_2):
    result = lib.simpson_method(test_func_2, 1, 0.5, 1.9, intervals=1000)
    assert abs(result - 1.94607) <= 0.1


def test_rectangle_method_1(test_func_1):
    result = lib.rectangle_method(test_func_1, 1, 0, 2*math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_rectangle_method_2(test_func_2):
    result = lib.rectangle_method(test_func_2, 1, 0.5, 1.9, intervals=10000)
    assert abs(result - 1.94607) <= 0.1


def test_trapezoid_method_1(test_func_1):
    result = lib.trapezoid_method(test_func_1, 1, 0, 2*math.pi, intervals=10000)
    assert abs(result - 82.683) <= 0.1


def test_trapezoid_method_2(test_func_2):
    result = lib.trapezoid_method(test_func_2, 1, 0.5, 1.9, intervals=10000)
    assert abs(result - 1.94607) <= 0.1
