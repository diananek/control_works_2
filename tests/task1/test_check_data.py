import src.util
from src import lib


def test_num_is_not_even():
    result = src.util.num_is_even(3)
    assert result is False


def test_num_is_even():
    result = src.util.num_is_even(2)
    assert result is True
