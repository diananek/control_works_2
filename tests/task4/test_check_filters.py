from src import lib
import numpy as np
import pandas as pd
import random


def test_correct_output_sma():
    array = [random.uniform(-10, 10) for i in range(100)]

    pd_array = pd.DataFrame(array)
    expected = pd_array.rolling(3, min_periods=1).mean()[0].to_list()
    expected = [float("{:.6f}".format(item)) for item in expected]

    result = lib.sma_filter(array, 3)
    result = [float("{:.6f}".format(item)) for item in result]
    assert result == expected


def test_correct_output_ema():
    array = [random.uniform(-10, 10) for i in range(100)]

    pd_array = pd.DataFrame(array)
    expected = pd.DataFrame.ewm(pd_array[0], span=3, adjust=False).mean().to_list()
    expected = [float("{:.6f}".format(item)) for item in expected]

    result = lib.ema_filter(array, 3)
    result = [float("{:.6f}".format(item)) for item in result]
    assert result == expected


def test_correct_output_smm():
    array = [random.uniform(-10, 10) for i in range(100)]

    pd_array = pd.DataFrame(array)
    expected = pd_array.rolling(3, min_periods=1).median()[0].to_list()
    expected = [float("{:.6f}".format(item)) for item in expected]

    result = lib.smm_filter(array, 3)
    result = [float("{:.6f}".format(item)) for item in result]
    assert result == expected
