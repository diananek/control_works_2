Repository for control works in second semester.
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

The src folder contains:
— a library with functions for tasks
— a module with utilities,
— folders for each task. Each such folder contains the main program for performing functions.

The tests folder contains all tests for tasks 1 and 4.
