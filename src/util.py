import matplotlib.pyplot as plt
from numpy import poly1d as np_poly1d, polyfit as np_polyfit


def num_is_even(num):
    if num % 2 == 0:
        return True
    else:
        return False

def get_lst_of_dif(method, intervals, f, a, x_low, x_up, ref_val):
    lst = []
    for i in intervals:
        cur_val = method(f, a, x_low, x_up, i)
        lst.append(abs(ref_val - cur_val))
    return lst
