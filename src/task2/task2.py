from src import lib
from src import util
import matplotlib.pyplot as plt

REF_VAL = 8623.34629386101
A = 1
X_LOW = 15
X_UP = 30

INTERVALS = [i for i in range(10000, 100000, 10000)]

result_trapezoid_method = util.get_lst_of_dif(lib.trapezoid_method, INTERVALS, lib.G, A, X_LOW, X_UP, REF_VAL)
result_rectangle_method = util.get_lst_of_dif(lib.rectangle_method, INTERVALS, lib.G, A, X_LOW, X_UP, REF_VAL)
result_simpson_method = util.get_lst_of_dif(lib.simpson_method, INTERVALS, lib.G, A, X_LOW, X_UP, REF_VAL)

plt.plot(INTERVALS, result_trapezoid_method, '-or', label='Метод трапеций')
plt.plot(INTERVALS, result_rectangle_method, '-ob', label='Метод прямоугольников')
plt.plot(INTERVALS, result_simpson_method, '-oc', label='Метод Симпсона')

plt.xlabel('количество интервалов')
plt.ylabel('разность между истинным значением и приближением')
plt.legend()

plt.show()

