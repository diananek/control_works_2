from src.util import num_is_even
import numpy as np
import collections
import statistics
import copy


def simpson_method(f, a, lower, upper, intervals=8):
    """
    Integration by the Simpson method

    :param f: function for integration
    :param a: coefficient for function
    :param lower: lower limit of integration
    :param upper: upper limit of integration
    :param intervals: number of splits
    :return: integral value
    """
    if num_is_even(intervals) and intervals >= 0:
        h = (upper - lower) / intervals
        z = intervals / 2
        temp = 0
        x = lower + h
        for i in range(1, int(z + 1)):
            res = f(a, x)
            temp += 4 * res
            x += 2 * h
        x = lower + 2 * h
        for i in range(1, int(z)):
            res = f(a, x)
            temp += 2 * res
            x += 2 * h
        res_low = f(a, lower)
        res_up = f(a, upper)
        ans = (h / 3) * (res_up + res_low + temp)
        return ans
    else:
        return None


def rectangle_method(f, a, lower, upper, intervals=20):
    """

    Integration by the method of rectangles

    :param f: function for integration
    :param a: coefficient for function
    :param lower: lower limit of integration
    :param upper: upper limit of integration
    :param intervals: number of splits
    :return: integral value
    """
    rectangle_edges=np.linspace(lower, upper, intervals)
    area = 0
    for r in range(len(rectangle_edges)):
        area += f(a, lower+r*(upper-lower)/intervals) * (upper-lower)/intervals
    return area


def trapezoid_method(f, a, lower, upper, intervals=8):
    """

    Integration by trapezoidal method

    :param f: function for integration
    :param a: coefficient for function
    :param lower: lower limit of integration
    :param upper: upper limit of integration
    :param intervals: number of splits
    :return: integral value
    """

    # calculating step size
    h = (upper - lower) / intervals

    # Finding sum
    integration = f(a, lower) + f(a, upper)

    for i in range(1, intervals):
        k = lower + i * h
        integration = integration + 2 * f(a, k)

    # Finding final integration value
    integration = integration * h / 2

    return integration


def G(a, x):
    numerator = 8 * (7 * a ** 2 - 61 * a * x + 40 * x ** 2)
    denominator = 18 * a ** 2 - 11 * a * x + x ** 2
    if denominator == 0:
        return None
    else:
        return numerator / denominator


def sma_filter(data, window=3):
    """
    Simple Moving Average (SMA) Filter
    :param window : sliding window value (default = 3)
    :param collections.deque queue : queue of input
    :param  data: filter list
    :return float: result of filter
    """
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue.append(lst[i])
        queue_length = len(queue)
        if queue_length > window:
            queue.popleft()
            queue_length -= 1
        if queue_length == 0:
            lst[i] = 0
        else:
            lst[i] = sum(queue) / queue_length
    return lst


def ema_filter(data, window=3):
    """
    Exponential Moving Average (EMA) Filter
    :param window : sliding window value (default = 3)
    :param collections.deque queue : queue of input
    :param data: filter list
    :return float: result of filter
    """
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue_length = len(queue)
        if queue_length == 0:
            queue.append(lst[i])
            pass
        elif queue_length > window:
            queue.popleft()
        alpha = 2 / (window + 1)
        lst[i] = (alpha * lst[i]) + ((1 - alpha) * queue[-1])
        queue.append(lst[i])
    return lst


def smm_filter(data, window=3):
    """
    Simple Moving Median (SMM) Filter
    :param window: sliding window value (default = 3)
    :param collections.deque queue: queue of input
    :param data: filter list
    :return float: result of filter
    """
    lst = copy.copy(data)
    queue = collections.deque()
    for i in range(len(lst)):
        queue.append(lst[i])
        queue_length = len(queue)
        if queue_length > window:
            queue.popleft()
            queue_length -= 1
        if queue_length == 0:
            median = 0
        else:
            median = statistics.median(queue)
            lst[i] = median
    return lst


