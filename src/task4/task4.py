from src.lib import sma_filter, smm_filter, ema_filter
import matplotlib.pyplot as plt
import xlrd

file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(17, start_rowx=1, end_rowx=2502)
data = [float(elem) for elem in data]


sma_result = sma_filter(data, 3)
smm_result = smm_filter(data, 3)
ema_result = ema_filter(data, 3)

fig = plt.figure()
plt.subplots_adjust(top=0.936, bottom=0.07, wspace=0.121, right=0.971, left=0.052)
fig.set_size_inches(11.69, 8.27, forward=True)

plt.subplot(3, 1, 1)
plt.title('Средняя простая скользящая')
plt.plot(data, 'k')
plt.plot(sma_result, 'y')

plt.subplot(3, 1, 2)
plt.title('Средняя экспоненциальная скользящая')
plt.plot(data, 'k')
plt.plot(ema_result, 'b')

plt.subplot(3, 1, 3)
plt.title('Медианный фильтр')
plt.plot(data, 'k')
plt.plot(smm_result, 'r')

plt.show()