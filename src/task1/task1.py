from src import lib

try:
    a = float(input('Введите значение а: '))
    x1 = float(input('Введите нижний предел интегрирования: '))
    x2 = float(input('Введите верхний предел интегрирования: '))
except ValueError:
    print('Ошибка ввода')
    exit(1)

if x1 < x2:
    print(f'Метод Симпсона: {lib.simpson_method(lib.G, a, x1, x2):.5f}')
    print(f'Метод прямоугольников: {lib.rectangle_method(lib.G, a, x1, x2):.5f}')
    print(f'Метод трапеций: {lib.trapezoid_method(lib.G, a, x1, x2):.5f}')
else:
    print('Ошибка ввода')
